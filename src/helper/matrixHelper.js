function getRandomInt(min, max) {
  return Math.round(Math.random() * (max - min)) + min;
}

export function generateRandomGraphMatrix(qty, chance) {
    const initialMatrix = new Array(qty).fill(0).map(() => new Array(qty).fill(0));

    for (let i = 0; i < initialMatrix.length; i++) {
        for (let j = i; j < initialMatrix[i].length; j++) {
            const rand = Math.random();
            if (rand <= chance && i !== j) {
                initialMatrix[i][j] = getRandomInt(1, 9);
            }
        }
    }

    return initialMatrix;
}

export function getGraphLinks(matrix, nodes) {
    const links = [];
    for (let i = 0; i < matrix.length; i++) {
        for (let j = 0; j < matrix[i].length; j++) {
            if (matrix[i][j]) {
                links.push(
                    {
                        source: nodes[i],
                        target: nodes[j],
                        value: Math.sqrt(matrix[i][j] * 2),
                    }
                );
            }
        }
    }

    return links;
}

export function getNodePosition(index, qty, circleRadius) {
    const angle = 2 / qty * index * Math.PI;
    const wrap = 400;
    const P = qty * circleRadius * 2;
    const radius = P / (2 * Math.PI) + 100;

    
    return {
        x: wrap + radius * Math.sin(angle),
        y: wrap - radius * Math.cos(angle),
    }
}