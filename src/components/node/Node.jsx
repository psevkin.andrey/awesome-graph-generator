import React from 'react';
import styled, {css} from 'styled-components';

const Circle = styled.circle`
  fill: lightblue;

  ${props => css`
    r: ${props.radius};
    cx: ${props.x};
    cy: ${props.y};
  `}
`;

const CircleTitle = styled.text`
  font-size: 18px;
  fill: black;
  transform: translate(-5px, 5px);
`;

const Node = ({x, y, index, radius}) => (
  <React.Fragment>
    <Circle x={x} y={y} radius={radius}/>
    <CircleTitle x={x} y={y}>{index}</CircleTitle>
  </React.Fragment>
);

export default Node;