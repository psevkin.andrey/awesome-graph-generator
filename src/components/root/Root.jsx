import React, { Component } from 'react';
import Graph from '../graph/Graph';
import CustomForm from '../../custom-form/CustomForm';

class Root extends Component {

  constructor(props) {
    super(props);

    this.state = {
      qty: 8,
      chance: 0.3,
      radius: 20,
    };
  }

  handleSubmit = ({qty, chance, radius}) => {
    this.setState({
        qty,
        chance,
        radius,
    });
};

  render() {
    const { qty, chance, radius } = this.state;
    return (
      <React.Fragment>
        <CustomForm onSubmit={this.handleSubmit}/>
        <Graph qty={+qty} chance={+chance} radius={+radius} />
      </React.Fragment>
    );
  }
}

export default Root;