import React, { Component } from 'react';
import styled, { css } from 'styled-components';
import Link from '../link/Link';
import Node from '../node/Node';

import {
  generateRandomGraphMatrix,
  getGraphLinks,
  getNodePosition,
} from '../../helper/matrixHelper';

const SvgArea = styled.svg`
  ${props => css`
    width: ${props.width};
    height ${props.height};
  `}
`;

class Graph extends Component {

  render() {
    const { qty, chance, radius } = this.props;
    const matrix = generateRandomGraphMatrix(qty, chance);
    const nodes = matrix.map((item, index) => getNodePosition(index, qty, radius));
    const links = getGraphLinks(matrix, nodes);

    return (
      <React.Fragment>
        <SvgArea width={1000} height={800}>
          {links.map(({source, target, value}, index) => <Link key={index} x1={source.x} y1={source.y} x2={target.x} y2={target.y} weight={value} />)}
          {nodes.map(({ x, y }, index) => <Node key={index} x={x} y={y} radius={radius} index={index} />)}
        </SvgArea>
        <textarea value={matrix.reduce((result, row) => `${result}${row.join(' ')}\n`, '')} cols="30" rows="10"></textarea>
      </React.Fragment>
    );
  }
}

Graph.defaultProps = {
  qty: 8,
  chance: 0.3,
};

export default Graph;