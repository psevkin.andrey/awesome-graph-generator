import React from 'react';
import styled, {css} from 'styled-components';

const Line = styled.line`
  stroke: black;

  ${({x1, y1, x2, y2, weight}) => css`
    x1: ${x1};
    y1: ${y1};
    x2: ${x2};
    y2: ${y2};
    stroke-width: ${weight}
  `}
`;

const Link = props => <Line {...props} />;

export default Link;