import React, { Component } from 'react';

class CustomForm extends Component {

    constructor(props) {
        super(props);

        this.state = {
            qty: 8,
            chance: 0.3,
            radius: 20,
        };
    }

    handleSubmit = e => {
        e.preventDefault();
        const {onSubmit} = this.props;
        const { qty, radius, chance } = this.state;

        onSubmit({qty, radius, chance});
    };

    onChange = (e, field) => {
        this.setState({
            [field]: e.target.value,
        });
    };

    render() {
        const { qty, chance, radius } = this.state;
        return (
            <form onSubmit={this.handleSubmit}>
                <label>
                    Quantity:
                    <input type="number" name="qty" value={qty} onChange={(e) => this.onChange(e, 'qty')} />
                </label>
                <label>
                    Chance:
                    <input type="number" name="chance" value={chance} onChange={(e) => this.onChange(e, 'chance')} />
                </label>
                <label>
                    Circle radius:
                    <input type="number" name="radius" value={radius} onChange={(e) => this.onChange(e, 'radius')} />
                </label>
                <button type="submit">Apply</button>
            </form>
        );
    }
}

export default CustomForm;
